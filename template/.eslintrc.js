module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "eslint:recommended",
        "google"
    ],
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 12,
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "ignorePatterns": [
      "index.js"
    ],
    "rules": {
     "indent": ["error", 2],
     'max-len': [2, 160, 2, {'ignoreUrls': true}],
    }
};
