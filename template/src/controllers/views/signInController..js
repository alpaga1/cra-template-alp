/**
 * @author
 */

import React from 'react';

import Controller from 'utils/controller.js';

class SignInController extends Controller {
  constructor() {
    super({});
  }
}

export default new SignInController();
