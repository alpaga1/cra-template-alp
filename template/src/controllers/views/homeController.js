/**
 * @author
 */

import React from 'react';

import Controller from 'utils/controller.js';
import {reducer, initState} from 'reducers/homeReducer.js';


const DEFAULT_PAGE = 1;

class HomeController extends Controller {
  constructor() {
    super({
      reducer: reducer,
      initState: initState,
      loading: false,
      attr: {
        switch: 0,
        page: DEFAULT_PAGE,
      }
    });
  }

  /* Optional function to upplaod model from db */
  _init = async (loaded) => {
    const timer = setTimeout(() => {
      loaded();
    }, 2000);
    return () => clearTimeout(timer);
  }

  /* Optional function to update attr from local page */
  useUpdateAttr() {
    React.useEffect(() => {
      console.log("Switch updated")
    }, [this.attr[0].switch]);

    React.useEffect(() => {
      console.log("Page updated")
    }, [this.attr[0].page]);
  }

  /* Actions function in your local page */
  hooks = {
    useUpdateToken: () => {
      this.dispatcher({type: "UPDATE_TOKEN", value: Math.floor(Math.random() * 50)});
    },

    useUpdateSwitch: () => {
      this.attr[1]({
        ...this.attr[0],
        switch: this.attr[0].switch == 1 ? 0 : 1
      });
    },

    useUpdatePage: () => {
      this.attr[1]({
        ...this.attr[0],
        page: this.attr[0].page + 1
      });
    }
  }
}

export default new HomeController();
