/**
 * @author
 */

import React from 'react';

/* 📦 Node modules */
import {Button, Container} from 'react-bootstrap';

import ctrl from 'controllers/views/homeController';

/**
 * View home page
 * @return {Object} React node
 */
function Home() {
  const {loading, state, attr} = ctrl.useInit();

  return (
    <Container fuild="true">
      <h1>Home {loading ? "Loading" : "loaded"}</h1>
      <Button onClick={() => ctrl.hooks.useUpdateToken()}>Generate</Button>
      <h4>Token: {state.token}</h4>
      <Button onClick={() => ctrl.hooks.useUpdateSwitch()}>Switch</Button>
      <h4>Switch: {attr.switch}</h4>
      <Button onClick={() => ctrl.hooks.useUpdatePage()}>Next</Button>
      <h4>Page: {attr.page}</h4>
    </Container>
  );
}

export default Home;
