/**
 * @author
 */

import React from 'react';

/* 📦 Node modules */
import {ThemeProvider} from 'styled-components';

/* 🎨 Theme available */
import themes from 'styles/themes';

/* 🔧 Utils */
import ReducerContext from 'utils/reducerContext.js';

/* 👁️‍🗨️ Hooks */
import {userContext} from 'hooks/userHooks';

/* 🗺️ Routes */
import routes from 'routes';

/* 📚 Libs */
import AlpCustomRouter from 'libs/AlpCustomRouter';

/**
 * Main page for react project
 * @return {Object} React DOM
 */
function App() {
  return (
    <React.Suspense fallback="Loading">
      <ThemeProvider theme={themes('light')}>
        <ReducerContext context={userContext}>
          <AlpCustomRouter routes={routes} isAuthenticated={false} />
        </ReducerContext>
      </ThemeProvider>
    </React.Suspense>
  );
}

export default App;
