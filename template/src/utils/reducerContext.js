/**
 * @autor
 */

import React from 'react';
import PropTypes from 'prop-types';

const contexts = {};

/**
 * Get state reducer
 * @param {string} key Context key
 * @return {object} State reducer
 */
export function useReducerContext(key) {
  if (!(key in contexts)) {
    throw new Error('[ReducerContext:useReducerContext] - Key undedined');
  }
  return React.useContext(contexts[key]);
}

/**
 * Create global reducer context
 * @param {object} param0 Context & children
 * @return {object} React Element
 */
function ReducerContext(props) {
  const {context, children} = props;
  const {key, reducer, initialState} = context;
  const [state, dispatch] = React.useReducer(reducer, initialState);
  contexts[key] = React.createContext(null);
  return React.createElement(
    contexts[key].Provider,
    {value: [state, dispatch]},
    children,
  );
}

ReducerContext.propTypes = {
  context: PropTypes.object,
  children: PropTypes.any,
};

export default ReducerContext;
