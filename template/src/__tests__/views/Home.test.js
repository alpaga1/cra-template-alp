/**
 * @author
 */

import React from 'react';
import { render } from '@testing-library/react';
import Home from 'views/Home.jsx';

test('Example Test on App.jsx', () => {
  const { getByText } = render(<Home />);

  expect(getByText(/Home/i)).toBeInTheDocument();
});
