/**
 * @author
 */

export const initState = {
  token: null,
}

export function reducer(state, action) {
  switch (action.type) {
    case "UPDATE_TOKEN":
      return {...state, token: action.value};
    default:
      return state;
  }
}