/**
 * @author
 */

/* Themes files */
import light from './light.js';
import dark from './dark.js';

/**
 * Get themes values
 * @param {String} theme Theme to use
 * @return {Object} Theme values
 */
function theme(theme = 'light') {
  const themes = {
    light: light,
    dark: dark,
  }
  return themes[theme] || light;
}

export default theme;
