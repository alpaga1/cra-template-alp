/**
 * @author
 */

const CONTEXT_KEY = 'user';

/* Your initialState for User log */
export const userInitialState = {
  auth: false,
};

/* Default user Context */
export const userContext = {
  key: CONTEXT_KEY,
  reducer: useReducer,
  initialState: userInitialState,
};

/**
 * User reducer
 * @param {object} state Current state
 * @param {object} action Action receive
 */
function useReducer(state, action) {
  switch (action.type) {
  default:
    throw new Error('[Reducer:user] - Action not defined');
  }
}
