/**
 * @author
 */

import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';

function AlpCustomRouter({routes, isAuthenticated}) {
  return (
    <Router>
      <Switch>
        {
          routes.map((route) => {
            return route.private == true ? 
              <PrivateRoute isAuthenticated={isAuthenticated} {...route}>
                <route.view/>
              </PrivateRoute> :
              <PublicRoute isAuthenticated={isAuthenticated} {...route}>
                <route.view/>
              </PublicRoute>
          })
        }
        <Route path="*"><h1>Not Found</h1></Route>
      </Switch>
    </Router>
  )
}

export default AlpCustomRouter;
