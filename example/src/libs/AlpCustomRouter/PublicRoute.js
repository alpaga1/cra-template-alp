/**
 * @author Alpaga
 */

import {Route, Redirect} from 'react-router-dom';
  
function PublicRoute({children, isAuthenticated, ...rest}) {
  console.log(children)
  return (
    <Route {...rest} render={({location}) => {
      return !isAuthenticated ? children : <Redirect to={{pathname: '/', state: {from: location}}}/>
    }}/>
  );
}

export default PublicRoute;
