/**
 * @author
 */

import React from 'react';

/**
 * Define all your routes
 * example: {
 *    path: {String} URL path
 *    view: {Object} Your React view
 *    exact: {Boolean} If the path is exact
 *    private: {Boolean} If user need to be login
 * };
 */
const routes = [
  {
    path: '/',
    view: React.lazy(() => import('views/Home')),
    exact: true,
    private: true,
  },
  {
    path: '/sign-in',
    view: React.lazy(() => import('views/SignIn')),
    exact: true,
    private: false,
  }
];

export default routes;
