/**
 * 🦙 Welcome to React App Template Alp !
 * =======================================
 *          Enjoy your new template
 * =======================================
 */

import React from 'react';
import ReactDOM from 'react-dom';

// 🌏 Global scss && translation
import 'styles/index.scss';
import 'utils/i18n.js';

// First view on React App
import App from 'views/App.jsx';

// 💥 Advanced configuration
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root'),
);

serviceWorker.unregister();
