/**
 * @author
 */

 import React from 'react';

 /* 📦 Node modules */
 import {Container} from 'react-bootstrap';
 
 import ctrl from 'controllers/views/homeController';
 
 /**
  * View sign-in page
  * @return {Object} React node
  */
 function SignIn() {
   ctrl.useInit();
 
   return (
     <Container fuild="true">
       <h1>SignIn</h1>
     </Container>
   );
 }
 
 export default SignIn;
 