/**
 * @author
 */

 import React from 'react';

class Controller {
  constructor(props = {}) {
    this.reducer = props.reducer || ((state) => {return state});
    this.initState = props.initState || null;
    this.loading = props.loading || false;
    this.attributes = props.attr || null;
  }

  useInit = (() => {
    const [state, dispatch] = React.useReducer(this.reducer, this.initState);
    const [loading, setLoading] = React.useState(this.loading);

    React.useEffect(() => {
      if (this._init) this._init(() => setLoading(false));
    }, []);

    this.dispatcher = dispatch;

    /* Build page attr */
    this.attr = React.useState(this.attributes);
    if (this.useUpdateAttr) this.useUpdateAttr();
  
    return {loading: loading, state: state, attr: this.attr[0]};
  })
}

export default Controller;
